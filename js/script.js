window.onload = function () {
  function removeTransition(e) {
    if (e.propertyName !== 'transform') return; // skip it if it's not a transform
    e.target.classList.remove('playing');
  }

  function playSound(e) {
    const audio = document.querySelector(`audio[data-key="${e.keyCode}"]`);
    const key = document.querySelector(`div[data-key="${e.keyCode}"]`);
    if (!audio) return; // stop the function from running all together

    key.classList.add('playing');
    audio.currentTime = 0; // rewind element to the start 
    audio.play();
  }

  const keys = Array.from(document.querySelectorAll('.key')); // traversal on all the elements not one
  keys.forEach(key => key.addEventListener('transitionend', removeTransition)); // add an event listener on each one of the elements selected in line 17
  window.addEventListener('keydown', playSound);
};

